package com.solonub.findmyage

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun btnFindAgeEvent(view: View) {
        if(TextUtils.isEmpty(etYearOfBirth.text)){
            tvMyAge.text = "Invalid Input"
            return
        }

        val yearOfBirth:Int = etYearOfBirth.text.toString().toInt()
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)

        if(yearOfBirth == 0 || yearOfBirth > currentYear){
            tvMyAge.text = "Invalid Input"
            return
        }

        val myAge = currentYear - yearOfBirth
        tvMyAge.text = "Your Age is $myAge year"
    }
}
